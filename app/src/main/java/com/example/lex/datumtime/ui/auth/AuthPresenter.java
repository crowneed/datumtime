package com.example.lex.datumtime.ui.auth;

import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lex on 12.09.18.
 */

@InjectViewState
public class AuthPresenter
        extends MvpPresenter<AuthView> {

    private String usersInJson;
    private List<User> users;
    private Gson gson;
    private SharedPreferences sharedPreferences;

    /**
     * В конструкторе вызываем метод createUsers
     * полученный список пользователей парсим в Json
     * и сохраняем в SharedPreferences
     * @param sharedPreferences
     */
    public AuthPresenter(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        gson = new GsonBuilder().create();
        usersInJson = sharedPreferences.getString(Constants.TAG_USERS_JSON,null);
        if (usersInJson == null) {
            createUsers();
            usersInJson = gson.toJson(users);
            this.sharedPreferences.edit()
                    .putString(Constants.TAG_USERS_JSON, usersInJson)
                    .apply();
        }
    }

    /**
     * Создаем пользователей и помещаем их в список users
     */
    private void createUsers() {
        users = new ArrayList<>();

        User user = new User();
        user.setLogin("user");
        user.setPassword("12345");
        user.setSaved(false);
        user.setAuth(false);
        user.setMarks(null);
        users.add(user);

        User admin = new User();
        admin.setLogin("admin");
        admin.setPassword("qwerty12+");
        admin.setSaved(false);
        admin.setAuth(false);
        admin.setMarks(null);
        users.add(admin);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if(usersInJson == null) return;
        usersInJson = sharedPreferences.getString(Constants.TAG_USERS_JSON,null);
        users = Arrays.asList(gson.fromJson(usersInJson, User[].class));
        for (User user : users) {
            if (user.isSaved()) {
                getViewState().login();
                return;
            }
        }
    }

    /**
     * Проверяем переданные логин и пароль
     * и вызываем showTime с текстом ошибки если они пустые
     * Далее проверяем существует ли в системе
     * такой логин и пароль
     * Если существует вызываем login
     * Если нет, то showTime с текстом ошибки
     * @param login
     * @param password
     */
    public void onLogin(String login,
                        String password,
                        boolean isSave) {
        if (login.isEmpty()
                || password.isEmpty()){
            getViewState().showMessage(Constants.ERROR_NULL);
            return;
        }

        for (User user : users) {
            if (login.equals(user.getLogin())
                    && password.equals(user.getPassword())) {
                if (isSave) {
                    user.setSaved(true);
                }
                user.setAuth(true);
                sharedPreferences.edit()
                        .putString(
                                Constants.TAG_USERS_JSON,
                                gson.toJson(users))
                        .apply();
                getViewState().login();
                return;
            }
        }
        getViewState().showMessage(Constants.ERROR_INVALIDATE);

    }
}
