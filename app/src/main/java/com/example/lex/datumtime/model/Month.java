package com.example.lex.datumtime.model;

import java.util.HashMap;
import java.util.Map;

public class Month {
    private Map<Integer, Day> days = new HashMap<>();
    private int monthOfYear;
    private int year;

    public Month(int amountDays,
                 int monthOfYear,
                 int year) {
        this.monthOfYear = monthOfYear;
        this.year = year;

        for (int i = 1; i <= amountDays; i++) {
            days.put(i, new Day(i, monthOfYear, year));
        }
    }

    public Map<Integer, Day> getDays() {
        return days;
    }

    public int getMonthOfYear() {
        return monthOfYear;
    }

    public int getYear() {
        return year;
    }

    public Day getDay(int i) {
        return days.get(i);
    }
}
