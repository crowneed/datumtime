package com.example.lex.datumtime.ui.workers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.ui.workers.adapters.WorkersAdapter;
import com.example.lex.datumtime.ui.workers.workersCalendar.WorkerCalendarActivity;
import com.example.lex.datumtime.model.Worker;

import java.util.List;

public class WorkersFragment
        extends MvpAppCompatFragment
        implements WorkersView {

    @InjectPresenter
    WorkersPresenter workersPresenter;

    private RecyclerView recyclerView;
    private WorkersAdapter workersAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_workers,null);
        init(view);
        return view;
    }

    /**
     * Инициализируем recyclerView
     */
    private void init(View view){
        recyclerView = view.findViewById(R.id.rv);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), manager.getOrientation()));

    }

    /**
     * Инициализируем адаптер и передаем в него
     * список @param workers
     */
    @Override
    public void initAdapter(List<Worker> workers) {
        workersAdapter = new WorkersAdapter(workers, new WorkersAdapter.OnWorkerClickListener() {
            @Override
            public void onWorkerClick(Worker worker) {
                workersPresenter.onWorkerClick(worker);
            }
        });
        recyclerView.setAdapter(workersAdapter);
        workersAdapter.notifyDataSetChanged();
    }

    @Override
    public void showCalendar(String jsonWorker) {
        Intent intent = new Intent(getActivity(), WorkerCalendarActivity.class);
        intent.putExtra(Constants.TAG_INTENT_WORKER, jsonWorker);

        startActivity(intent);
    }
}
