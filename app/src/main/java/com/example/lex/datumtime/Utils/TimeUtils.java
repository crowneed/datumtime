package com.example.lex.datumtime.Utils;

import com.example.lex.datumtime.model.Day;
import com.example.lex.datumtime.model.Month;
import com.example.lex.datumtime.model.Year;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TimeUtils {

    public static long longToHours(long time) {
        return time/(1000*60*60) % 60;
    }
    public static long longToMinutes(long time) {
        return time/(1000*60) % 60;
    }
    public static long longToSeconds(long time) {
        return time/(1000) % 60;
    }

    //DAY
    public static long getDayTimeLong(Day day){
        List<Date> marks = day.getMarks();
        Calendar calendar = Calendar.getInstance();
        if (marks.size() == 0) { return 0; }

        int sizeMarks = marks.size();

        if (marks.size() % 2 != 0) {
            calendar.setTime(marks.get(0));
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == dayOfMonth){
                sizeMarks--;
            } else {
                addCheckOut(day);
            }
        }

        long inMark = 0;
        long outMark = 0;

        for (int i = 0; i < sizeMarks; i++) {
            if (i % 2 == 0) inMark += marks.get(i).getTime();
            else outMark += marks.get(i).getTime();
        }

        return outMark - inMark;
    }

    private static void addCheckOut(Day day){
        List<Date> marks = day.getMarks();
        Calendar lastMark = Calendar.getInstance();
        Calendar autoCheckPoint = Calendar.getInstance();
        lastMark.setTime(marks.get(marks.size()-1));

        int dayOfMonth = lastMark.get(Calendar.DAY_OF_MONTH);
        int month = lastMark.get(Calendar.MONTH);
        int year = lastMark.get(Calendar.YEAR);
        autoCheckPoint.set(year, month, dayOfMonth,18,0);
        if(autoCheckPoint.after(lastMark)){
            marks.add(autoCheckPoint.getTime());
        } else {
            Date newTime = new Date();
            newTime.setTime(lastMark.getTime().getTime() + 60000);
            marks.add(newTime);
        }
    }

    public static String getTimeStringFromLong(long time){
        long hours = longToHours(time);
        long minutes = longToMinutes(time);

        String dayTime;
        if (minutes == 0){
            dayTime = String.valueOf(hours)
                    + "ч";
        } else if (hours == 0) {
            dayTime = String.valueOf(minutes)
                    + "м";
        } else {
            dayTime = String.valueOf(hours)
                    + "ч "
                    + String.valueOf(minutes)
                    + "м";
        }
        return dayTime;
    }

    public static String getTimeinChronoFormat(long time){
        long h = longToHours(time);
        long m = longToMinutes(time);
        long s = longToSeconds(time);

        String hh = h == 0 ? "" : h + "ч ";
        String mm = m == 0 ? "" : m + "м ";
        String ss = s + "с";
        String dayTime = "Текущий день: " + hh + mm + ss;
        return dayTime;
    }

    public static String getDayTimeString(Day day) {
        return getTimeStringFromLong(getDayTimeLong(day));
    }

    public static long getHoursOfDay(Day day){
        return longToHours(getDayTimeLong(day));
    }

    //ВЕРОЯТНО DEL
    public String getMarksOfDayString(Day day){
        List<Date> marks = day.getMarks();
        Calendar calendar = Calendar.getInstance();
        String result = "";

        if (marks.size() == 0) {
            return "За этот день нет ни одной отметки";
        }

        int sizeMarks = marks.size();

        if (marks.size() % 2 != 0) {
            calendar.setTime(marks.get(0));
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) != dayOfMonth){
                addCheckOut(day);
            }
        }

        long inMark = 0;
        long outMark = 0;
        String in ="";
        String out ="";

        for (int i = 0; i < sizeMarks; i++) {
            calendar.setTime(marks.get(i));
            String temp = calendar.get(Calendar.HOUR_OF_DAY)
                    + ":"
                    + calendar.get(Calendar.MINUTE);

            if (i % 2 == 0) {
                in = temp;
                inMark = marks.get(i).getTime();
                result = result.concat(in
                        + " - ");
            } else {
                out = temp;
                outMark = marks.get(i).getTime();

                temp = getTimeStringFromLong(outMark-inMark);

                result = result.concat(out
                        + "\n"
                        + "Итого: "
                        + temp
                        + "\n\n");
            }

        }
        return result;
    }

    // MONTH
    public static String getMonthTimeString(Month month){
        long monthTimeLong = getMonthTimeLong(month);
        String monthTime = getTimeStringFromLong(monthTimeLong);
        monthTime = monthTime + "/" + getWorkTimeOfMonth(month) + "ч";
        return monthTime;
    }

    private static int getWorkTimeOfMonth(Month month){
        Map<Integer, Day> days = month.getDays();
        int year = month.getYear();
        int monthOfYear = month.getMonthOfYear();
        int countWorkDays = 0;

        for(Map.Entry<Integer, Day> dayofMonth : days.entrySet()){
            LocalDate calendarDay = LocalDate.of(year, monthOfYear, dayofMonth.getKey());
            if(calendarDay.getDayOfWeek() != DayOfWeek.SUNDAY
                    && calendarDay.getDayOfWeek() != DayOfWeek.SATURDAY){
                countWorkDays++;
            }
        }
        return countWorkDays * 8;
    }

    public static long getMonthTimeLong(Month month) {
        Map<Integer, Day> days = month.getDays();
        long monthTimeLong = 0;
        for(Map.Entry<Integer, Day> dayofMonth : days.entrySet()){
            monthTimeLong += getDayTimeLong(dayofMonth.getValue());
        }
        return monthTimeLong;
    }

    //YEAR
    public static CalendarDay getFirstWorkDayOfYear(Year year) {
        if (year == null) return null;
        Map<Integer, Month> months= year.getMonths();
        int month = 13;
        int day = 32;

        for (Map.Entry<Integer, Month> monthsOfYear : months.entrySet()) {
            if (getMonthTimeLong(monthsOfYear.getValue()) == 0) continue;
            if (monthsOfYear.getKey() < month) month = monthsOfYear.getKey();
        }

        if (month == 13) return null;

        for (Map.Entry<Integer, Day> dayOfMonth : year.getMonth(month).getDays().entrySet()){
            if (getDayTimeLong(dayOfMonth.getValue()) == 0) continue;
            if (dayOfMonth.getKey() < day) day = dayOfMonth.getKey();
        }

        if (day == 32) return null;

        return CalendarDay.from(year.getYear(), month, day);
    }
}
