package com.example.lex.datumtime.ui.calendar.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.TimeUtils;
import com.example.lex.datumtime.model.Day;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MarksAdapter
        extends RecyclerView.Adapter<MarksAdapter.ViewHolder> {

    private List<Date> marks;
    private Day day;

    public MarksAdapter(Day day) {
        this.day = day;
        marks = day.getMarks();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
                                         int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_mark,
                        viewGroup,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,
                                 int i) {
        int indexMarks = i * 2;
        Date start = marks.get(indexMarks);
        viewHolder.start.setText(getStringMark(start));

        if (indexMarks + 1 < marks.size()) {
            Date end = marks.get(indexMarks + 1);
            viewHolder.end.setText(getStringMark(end));

            viewHolder.dif.setVisibility(View.VISIBLE);
            viewHolder.dif.setText(getDifMarks(start, end));
        } else {
            viewHolder.end.setText("");
            viewHolder.dif.setText("");
        }
    }

    private String getStringMark(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String time = calendar.get(Calendar.HOUR_OF_DAY)
                + ":";
        int minutes = calendar.get(Calendar.MINUTE);
        if (minutes < 10) {
            time = time
                    + 0
                    + String.valueOf(minutes);
        } else {
            time = time
                    + String.valueOf(minutes);
        }
        return time;
    }

    private String getDifMarks(Date start, Date end){
        long dif = end.getTime() - start.getTime();

        String time = Constants.IN_TOTAL;
        long minutes = TimeUtils.longToMinutes(dif);
        long hours = TimeUtils.longToHours(dif);
        if (minutes == 0){
            time = time
                    + String.valueOf(hours)
                    + Constants.TIME_HOUR;
        } else if (hours == 0) {
            time = time
                    + String.valueOf(minutes)
                    + Constants.TIME_MINUTE;
        } else {
            time = time
                    + String.valueOf(hours)
                    + Constants.TIME_HOUR
                    + " "
                    + String.valueOf(minutes)
                    + Constants.TIME_MINUTE;
        }
        return time;
    }

    @Override
    public int getItemCount() {
        int size = day.getMarks().size();
        if (size % 2 != 0)
            return size / 2 + 1;
        else
            return size / 2;
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder {
        private TextView start, end, dif;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            start = itemView.findViewById(R.id.tv_start);
            end = itemView.findViewById(R.id.tv_end);
            dif = itemView.findViewById(R.id.tv_dif);
        }
    }
}
