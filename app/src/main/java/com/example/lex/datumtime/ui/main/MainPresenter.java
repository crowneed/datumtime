package com.example.lex.datumtime.ui.main;

import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;

@InjectViewState
public class MainPresenter
        extends MvpPresenter<MainView> {

    private SharedPreferences sharedPreferences;
    private Gson gson;

    public MainPresenter(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        gson = new GsonBuilder().create();
        getViewState().startNotify();
    }

    /**
     * При первом старте передаем в MainActivity
     * показать первый пункт меню
     */
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showFragment(Constants.POS_MAP_FRAG);
    }

    /**
     * передаем в MainActivity команду
     * показать диалог
     */
    public void onBackPressed(boolean visibility) {
        getViewState().showExitDialog(visibility);
    }

    /**
     * передаем в MainActivity команду
     * показать пункт меню под номером
     * @param pos
     */
    public void onTab(int pos){
        getViewState().showFragment(pos);
    }

    @Override
    public void onDestroy() {
        String jsonUsers = sharedPreferences.getString(Constants.TAG_USERS_JSON, null);
        List<User> users = gson.fromJson(jsonUsers, new TypeToken<List<User>>(){}.getType());
        for(User user: users){
            if (user.isAuth()) {
                if(!user.isSaved()) {
                    user.setAuth(false);
                }
                break;
            }
        }
        jsonUsers = gson.toJson(users);
        sharedPreferences.edit().putString(Constants.TAG_USERS_JSON, jsonUsers).apply();
        super.onDestroy();
    }

    public void onExit(boolean visibility) {
        getViewState().showLogoutDialog(visibility);
    }

    public void onLogout() {
        String jsonUsers = sharedPreferences.getString(Constants.TAG_USERS_JSON, null);
        List<User> users = gson.fromJson(jsonUsers, new TypeToken<List<User>>(){}.getType());
        for(User user: users){
            if (user.isAuth()) {
                user.setSaved(false);
                break;
            }
        }
        jsonUsers = gson.toJson(users);
        sharedPreferences.edit().putString(Constants.TAG_USERS_JSON, jsonUsers).apply();
    }
}
