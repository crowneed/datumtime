package com.example.lex.datumtime.ui.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface MainView
        extends MvpView {

    /**
     * Показываем диалог с уточнением о выходе
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showExitDialog(boolean visibility);

    /**
     * Показываем пункт меню под номером @param pos
     */
    @StateStrategyType(SkipStrategy.class)
    void showFragment(int pos);

    @StateStrategyType(SkipStrategy.class)
    void startNotify();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showLogoutDialog(boolean visibility);
}
