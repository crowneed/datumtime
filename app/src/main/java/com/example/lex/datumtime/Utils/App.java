package com.example.lex.datumtime.Utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class App extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext(){
       return context;
    }

    public static SharedPreferences getSharedPreferences(String tag){
        return context.getSharedPreferences(
                tag,
                Context.MODE_PRIVATE);
    }
}
