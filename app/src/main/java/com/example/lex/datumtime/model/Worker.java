package com.example.lex.datumtime.model;

public class Worker {
    private String name;
    private Year marks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Year getMarks() {
        return marks;
    }

    public void setMarks(Year marks) {
        this.marks = marks;
    }
}
