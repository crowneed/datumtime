package com.example.lex.datumtime.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.ui.auth.AuthActivity;

import java.util.Calendar;

public class TimeNotification extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(Constants.NOTIF_INTENT_START_DAY)){
            sendNotification(context,
                    Constants.ID_START_DAY,
                    Constants.TEXT_START_DAY);
        } else if (action.equals(Constants.NOTIF_INTENT_END_DAY)){
            sendNotification(context,
                    Constants.ID_END_DAY,
                    Constants.TEXT_END_DAY);
        }
    }

    private void sendNotification(Context context,
                                  int id,
                                  String text) {
        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SATURDAY
                || dayOfWeek == Calendar.SUNDAY) return;

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager.getNotificationChannel(Constants.CHANNEL_NOTIF) == null){
                NotificationChannel notifChannel = new NotificationChannel(Constants.CHANNEL_NOTIF,
                        Constants.CHANNEL_NOTIF,
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(notifChannel);
            }
        }

        Intent intent = new Intent(context, AuthActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification notif = new NotificationCompat.Builder(context, Constants.CHANNEL_NOTIF)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_notification)
//                .setColor(App.getContext().getResources().getColor(R.color.colorWhite))
                .setTicker(text)
                .setContentText(text)
                .setContentTitle(Constants.TITLE_NOTIF)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(id, notif);
    }
}
