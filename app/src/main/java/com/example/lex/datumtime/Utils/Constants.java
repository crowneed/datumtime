package com.example.lex.datumtime.Utils;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.model.ColorInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Constants {
    public static final List<ColorInfo> colors = new ArrayList<>();
    static {
        colors.add(new ColorInfo(R.color.calendar_green, "Отработано 8+ часов"));
        colors.add(new ColorInfo(R.color.calendar_red, "Отработано < 8 часов"));
        colors.add(new ColorInfo(R.color.calendar_black, "Не вышел на работу"));
        colors.add(new ColorInfo(R.color.calendar_yellow, "Выходной день"));
        colors.add(new ColorInfo(R.color.calendar_blue, "Текущий день"));
    }

    public static final String TAG_SHARED_PREFERENCES = "datumTime";
    public static final String TAG_USERS_JSON = "usersJson";

    public static final String IN_TOTAL = "Итого: ";
    public static final String TIME_HOUR = "ч";
    public static final String TIME_MINUTE = "м";

    //Auth
    public static final String ERROR_NULL = "Все поля должны быть заполнены";
    public static final String ERROR_INVALIDATE = "Неправильный логин или пароль";
    public static final int PERMISSION_REQUEST = 1;

    //Calendar
    public static final String CURRENT_DAY = "Текущий день";
    public static final String WEEKEND_DAY = "Это выходной день";
    public static final String WORKED_PER_DAY_MARKS = "Всего за день отработано: ";
    public static final String NULL_MARKS = "За этот день нет ни одной отметки";
    public static final int CALENDAR_MIN_MONTH = 1;
    public static final int CALENDAR_MAX_MONTH = 12;
    public static final int CALENDAR_MIN_DAY = 1;
    public static final int CALENDAR_MAX_DAY = 31;

    public static final String TAG_INTENT_WORKER = "WORKER";

    public static final String WORKED_PER_DAY = "Отработано за день: ";
    public static final String WORKED_PER_MONTH = "Отработано за месяц: ";

    //Main
    public static final String NOTIF_INTENT_START_DAY = "START";
    public static final String NOTIF_INTENT_END_DAY = "END";
    public static final int NOTIF_START_DAY_HOUR = 9;
    public static final int NOTIF_START_DAY_MINUTE = 0;
    public static final int NOTIF_END_DAY_HOUR = 18;
    public static final int NOTIF_END_DAY_MINUTE = 0;

    public static final String DIALOG_EXIT_TITLE = "Выход";
    public static final String DIALOG_EXIT_MESSAGE = "Закрыть приложение?";
    public static final String DIALOG_LOGOUT_TITLE = "Выйти из учетной записи";
    public static final String DIALOG_LOGOUT_MESSAGE = "До свидания, Datum Time. " +
            "\nВы точно хотите закрыть приложение и выйти из своей учетной записи?";
    public static final String DIALOG_POSITIVE = "Да";
    public static final String DIALOG_NEGATIVE = "Нет";

    public static final int POS_MAP_FRAG = 1;
    public static final int POS_CALENDAR_FRAG = 2;
    public static final int POS_WORKERS_FRAG = 3;
    public static final int POS_EXIT = 4;

    //Map
    public static final String CHECK_OUT = "Отметить уход";
    public static final String CHECK_IN = "Отметить приход";
    public static final int STROKE_ZONE_WIDTH = 2;

    public static final double CENTER_ZONE_LAT = 47.226233;
    public static final double CENTER_ZONE_LNG = 39.696477;
    public static final int ZONE_RADIUS_IN_METERS = 40000; // должно быть 120
//    public static final int ZONE_RADIUS_IN_METERS = 120;
    public static final int AVERAGE_INTERVAL_IN_MS = 10000;
    public static final int MAX_INTERVAL_IN_MS = 5000;
    public static final int MAX_CIRCLE_DEGREE = 360;
    public static final int MIN_POINT_DEGREE = 3;
    public static final int DEFAULT_ZOOM_LEVEL = 17;

    public static final String MAIL_SENDER = "cloackqulity@gmail.com";
    public static final String MAIL_SENDER_PASSWORD = "Wc2tftljnf";
    public static final String MAIL_MESSAGE_TITLE = "Причина отсутствия";
    public static final String MAIL_MESSAGE_RECIPIENT = "develop37@datum-group.ru";
    public static final String MAIL_SENDED = "Сообщение отправлено";
    public static final String MAIL_NOT_SENDED = "Сообщение не отправлено";

    //Workers
    public static final String TIME_ARRIVAL = "Пришел в ";

    //NotificationReceiver
     public static final String TITLE_NOTIF = "Напоминание";
     public static final String TEXT_START_DAY = "Не забудьте отметить приход";
     public static final String TEXT_END_DAY = "Не забудьте отметить уход";
     public static final String CHANNEL_NOTIF = "CHANNEL_NOTIF";
     public static final int ID_START_DAY = 0;
     public static final int ID_END_DAY = 1;

}
