package com.example.lex.datumtime.ui.calendar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.lex.datumtime.R;

import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.TimeUtils;
import com.example.lex.datumtime.Utils.calendarUltis.ViewDecorator;
import com.example.lex.datumtime.model.Day;
import com.example.lex.datumtime.ui.calendar.adapters.ColorsAdapter;
import com.example.lex.datumtime.ui.calendar.adapters.MarksAdapter;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.Calendar;
import java.util.List;

public class CalendarFragment
        extends MvpAppCompatFragment
        implements MyCalendarView {

    @InjectPresenter
    CalendarPresenter calendarPresenter;

    private MaterialCalendarView calendar;
    private TextView tvDay, tvMonth, tvInfDay;
    private FloatingActionButton fab_colors;

    @ProvidePresenter
    CalendarPresenter provideCalendarPresenter(){
        Context context = getActivity().getApplicationContext();

        return new CalendarPresenter(
                context.getSharedPreferences(
                        Constants.TAG_SHARED_PREFERENCES,
                        Context.MODE_PRIVATE),
                this.getArguments());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar,null);
        init(view);
        return view;
    }

    /**
     * Инициализируем все поля CalendarFragment
     * @param view
     */
    private void init(View view) {
        calendar = view.findViewById(R.id.calendar);
        calendar.setSelectedDate(LocalDate.now());
        fab_colors = view.findViewById(R.id.fab_colors);
        fab_colors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarPresenter.onColorsClick(true);
            }
        });
        tvDay = view.findViewById(R.id.tv_day);
        tvMonth = view.findViewById(R.id.tv_month);
        tvInfDay = view.findViewById(R.id.tv_inf_day);
        tvInfDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarPresenter.onInfDayClick(calendar.getSelectedDate());
            }
        });

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        calendar.state().edit()
                .setMinimumDate(CalendarDay.from(currentYear,
                        Constants.CALENDAR_MIN_MONTH,
                        Constants.CALENDAR_MIN_DAY))
                .setMaximumDate(CalendarDay.from(currentYear,
                        Constants.CALENDAR_MAX_MONTH,
                        Constants.CALENDAR_MAX_DAY))
                .commit();

        calendar.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                calendarPresenter.onMonthChanged(calendarDay
                        .getMonth());
            }
        });
        calendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                calendarPresenter.onDateSelected(calendarDay);
            }
        });
    }

    @Override
    public void showTime(String day, String month) {
        tvDay.setText(Constants.WORKED_PER_DAY + day);
        tvMonth.setText(Constants.WORKED_PER_MONTH + month);
        tvInfDay.setVisibility(View.VISIBLE);
    }

    @Override
    public void addDecorators(List<CalendarDay> weekendDays,
                              List<CalendarDay> nullDays,
                              List<CalendarDay> badDays,
                              List<CalendarDay> goodDays,
                              CalendarDay currentDay) {
        calendar.removeDecorators();

        calendar.addDecorator(new ViewDecorator(getActivity(), R.color.calendar_yellow, weekendDays));
        calendar.addDecorator(new ViewDecorator(getActivity(), R.color.calendar_black, nullDays));
        calendar.addDecorator(new ViewDecorator(getActivity(), R.color.calendar_red, badDays));
        calendar.addDecorator(new ViewDecorator(getActivity(), R.color.calendar_green, goodDays));
        calendar.addDecorator(new ViewDecorator(getActivity(), R.color.calendar_blue, currentDay));
        calendar.invalidateDecorators();
    }

    @Override
    public void showMarks(final Day day, final String msg, boolean visibility) {
        if (visibility) {

            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
            View parentView = getLayoutInflater().inflate(R.layout.bottom_sheet_marks, null);
            bottomSheetDialog.setContentView(parentView);

            TextView tvDayTime = parentView.findViewById(R.id.tv_time_day);
            tvDayTime.setVisibility(View.VISIBLE);
            tvDayTime.setText(msg);

            RecyclerView rv = parentView.findViewById(R.id.rv_marks);
            rv.setVisibility(View.VISIBLE);
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
//            LinearLayoutManager manager;
//            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                // In landscape
//                manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//            } else {
//                // In portrait
//                manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//
//            }
            rv.setLayoutManager(manager);
            rv.addItemDecoration(new DividerItemDecoration(getActivity(), manager.getOrientation()));
            MarksAdapter adapter = new MarksAdapter(day);
            rv.setAdapter(adapter);

            bottomSheetDialog.show();
            bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    calendarPresenter.getViewState().showMarks(day, msg, false);
                }
            });
        }
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showColorsDialog(boolean visibility) {
        if (visibility){
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
            View parentView = getLayoutInflater().inflate(R.layout.bottom_sheet_colors, null);
            RecyclerView rvColors = parentView.findViewById(R.id.rv_colors);
//            LinearLayoutManager layoutManager;
//            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                // In landscape
//                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//            } else {
//                // In portrait
//                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//
//            }
            rvColors.setLayoutManager(new LinearLayoutManager(getActivity()));
//            rvColors.setLayoutManager(layoutManager);
            rvColors.setAdapter(new ColorsAdapter(Constants.colors));
            bottomSheetDialog.setContentView(parentView);
            bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                   calendarPresenter.onColorsClick(false);
                }
            });
            bottomSheetDialog.show();
        }

    }
}
