package com.example.lex.datumtime.ui.workers.adapters;

import java.util.Calendar;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.TimeUtils;
import com.example.lex.datumtime.model.Day;
import com.example.lex.datumtime.model.Worker;

import java.util.List;

public class WorkersAdapter
        extends RecyclerView.Adapter<WorkersAdapter.ViewHolder> {

    public interface OnWorkerClickListener{
        void onWorkerClick(Worker worker);
    }

    private List<Worker> workers;
    private final OnWorkerClickListener listener;

    public WorkersAdapter(List<Worker> workers,
                          OnWorkerClickListener listener) {
        this.workers = workers;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
                                         int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_worker,
                        viewGroup,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,
                                 int i) {
        final Worker curWorker = workers.get(i);
        Resources resources = viewHolder.itemView.getResources();
        viewHolder.name.setText(curWorker.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onWorkerClick(curWorker);
            }
        });

        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Day curDay = curWorker.getMarks().getMonth(month).getDay(day);
        if (curDay.getMarks().size() == 0) {
            viewHolder.itemView.setBackgroundColor(resources.getColor(R.color.workers_null));
        } else if (curDay.getMarks().size() % 2 == 0) {
            viewHolder.time.setText(Constants.WORKED_PER_DAY + TimeUtils.getDayTimeString(curDay));
        } else {
            calendar.setTime(curDay.getMarks().get(0));
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            int minutes = calendar.get(Calendar.MINUTE);
            String timeArrival = Constants.TIME_ARRIVAL + hours + ":";
            if (minutes < 10) timeArrival = timeArrival + 0 + minutes;
            else timeArrival = timeArrival + minutes;
            viewHolder.time.setText(timeArrival);
        }
    }

    @Override
    public int getItemCount() {
        return workers.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder {
        private TextView name, time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
            time = itemView.findViewById(R.id.tv_time);
        }
    }
}
