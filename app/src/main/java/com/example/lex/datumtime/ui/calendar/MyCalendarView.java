package com.example.lex.datumtime.ui.calendar;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.lex.datumtime.model.Day;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.List;

public interface MyCalendarView
        extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showTime(String day, String month);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void addDecorators(List<CalendarDay> weekendDays,
                       List<CalendarDay> nullDays,
            List<CalendarDay> badDays,
            List<CalendarDay> goodDays,
            CalendarDay currentDay);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showMarks(Day day, String msg, boolean visibility);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showColorsDialog(boolean visibility);

    @StateStrategyType(SkipStrategy.class)
    void showMessage(String msg);
}
