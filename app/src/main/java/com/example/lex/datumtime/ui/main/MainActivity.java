package com.example.lex.datumtime.ui.main;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.ui.calendar.CalendarFragment;
import com.example.lex.datumtime.ui.map.MapFragment;
import com.example.lex.datumtime.ui.workers.WorkersFragment;
import com.example.lex.datumtime.Utils.TimeNotification;

import org.osmdroid.config.Configuration;

import java.util.Calendar;


public class MainActivity
        extends MvpAppCompatActivity
        implements MainView {

    private AlertDialog.Builder exitDialog;
    private AlertDialog.Builder logoutDialog;
    private BottomNavigationView btmNavigView;

    @InjectPresenter
    MainPresenter mainPresenter;

    @ProvidePresenter
    MainPresenter provideMainPresenter(){
        return new MainPresenter(getSharedPreferences(Constants.TAG_SHARED_PREFERENCES,
                MODE_PRIVATE));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startNotify();
        init();
    }

    /**
     * Инициализируем все поля MainActivity
     * При нажатии на один из пунктов меню
     * передаем в MainPresenter номер нажатого пункта
     */
    private void init() {
        btmNavigView = findViewById(R.id.bottom_navigation);
        btmNavigView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        boolean isSelected = true;
                        int posTab = 0;
                        switch (menuItem.getItemId()){
                            case R.id.action_map:
                                posTab = Constants.POS_MAP_FRAG;
                                break;
                            case R.id.action_calendar:
                                posTab = Constants.POS_CALENDAR_FRAG;
                                break;
                            case R.id.action_workers:
                                posTab = Constants.POS_WORKERS_FRAG;
                                break;
                            case R.id.action_exit:
                                posTab = Constants.POS_EXIT;
                                isSelected = false;
                                break;
                        }
                        mainPresenter.onTab(posTab);
                        return isSelected;
                    }
                });
    }

    /**
     * Позываем диалог с уточнением о выходе из приложения
     */
    @Override
    public void showExitDialog(boolean visibility) {
        if (visibility) {
            exitDialog = new AlertDialog.Builder(this);
            exitDialog.setTitle(Constants.DIALOG_EXIT_TITLE);
            exitDialog.setMessage(Constants.DIALOG_EXIT_MESSAGE);
            exitDialog.setPositiveButton(Constants.DIALOG_POSITIVE,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            exitDialog.setNegativeButton(Constants.DIALOG_NEGATIVE,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });

            exitDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mainPresenter.onBackPressed(false);
                }
            });

            exitDialog.show();
        }
    }

    /**
     * Позываем пункт меню под номером @param pos
     */
    @Override
    public void showFragment(int pos) {

        Fragment fragment = null;
        switch (pos) {
            case Constants.POS_MAP_FRAG:
                fragment = new MapFragment();
                break;
            case Constants.POS_CALENDAR_FRAG:
                fragment = new CalendarFragment();
                break;
            case Constants.POS_WORKERS_FRAG:
                fragment = new WorkersFragment();
                break;
            case Constants.POS_EXIT:
                mainPresenter.onExit(true);
                break;
        }
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_holder, fragment)
                    .commit();
        }
    }

    /**
     * Устанавливаем время срабатывания уведомления
     * и создаем "будильники" для утреннего и вечернего уведомления
     */
    @Override
    public void startNotify() {
        createAlarm(Constants.NOTIF_START_DAY_HOUR,
                Constants.NOTIF_START_DAY_MINUTE,
                Constants.NOTIF_INTENT_START_DAY);
        createAlarm(Constants.NOTIF_END_DAY_HOUR,
                Constants.NOTIF_END_DAY_MINUTE,
                Constants.NOTIF_INTENT_END_DAY);
    }

    @Override
    public void showLogoutDialog(boolean visibility) {
        if (visibility) {
            logoutDialog = new AlertDialog.Builder(this);
            logoutDialog.setTitle(Constants.DIALOG_LOGOUT_TITLE);
            logoutDialog.setMessage(Constants.DIALOG_LOGOUT_MESSAGE);
            logoutDialog.setPositiveButton(Constants.DIALOG_POSITIVE,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mainPresenter.onLogout();
                            finish();
                        }
                    });
            logoutDialog.setNegativeButton(Constants.DIALOG_NEGATIVE,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });

            logoutDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mainPresenter.onExit(false);
                }
            });
            logoutDialog.show();
        }
    }

    /**
     * Создаем будильник для уведомления,
     * который сработает в @param alarmTime
     * с идентификатором интента @param intentAction
     */
    private void createAlarm(int hourAlarm, int minuteAlarm, String intentAction) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Calendar alarmTime = Calendar.getInstance();
        alarmTime.set(Calendar.HOUR_OF_DAY, hourAlarm);
        alarmTime.set(Calendar.MINUTE, minuteAlarm);
        alarmTime.set(Calendar.SECOND, 0);

        Calendar now = Calendar.getInstance();

        if(alarmTime.before(now)){
            alarmTime.add(Calendar.DAY_OF_MONTH,1);
        }

        Intent intent = new Intent(this, TimeNotification.class);
        intent.setAction(intentAction);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    /**
     * Передаем событие о нажатию кнопки "назад"
     * в MainPresenter
     */
    @Override
    public void onBackPressed() {
        mainPresenter.onBackPressed(true);
    }

}
