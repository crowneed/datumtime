package com.example.lex.datumtime.ui.auth;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by lex on 12.09.18.
 */

public interface AuthView
        extends MvpView {

    /**
     * Показываем пользователю некое сообщение
     * @param message
     */
    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);

    /**
     * Авторизация прошла успешно, переходим в MainActivity
     */
    @StateStrategyType(SkipStrategy.class)
    void login();

}
