package com.example.lex.datumtime.ui.calendar;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.lex.datumtime.Utils.App;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.TimeUtils;
import com.example.lex.datumtime.model.Day;
import com.example.lex.datumtime.model.Month;
import com.example.lex.datumtime.model.User;
import com.example.lex.datumtime.model.Worker;
import com.example.lex.datumtime.model.Year;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@InjectViewState
public class CalendarPresenter
        extends MvpPresenter<MyCalendarView> {
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private Calendar calendar;
    private Year curYear;
    private List<CalendarDay> badDays;
    private List<CalendarDay> weekendDays;
    private List<CalendarDay> goodDays;
    private List<CalendarDay> nullDays;
    private CalendarDay currentDay;


    public CalendarPresenter(SharedPreferences sharedPreferences, Bundle bundle) {
        init(sharedPreferences, bundle);
        fillCalendar(calendar.get(Calendar.MONTH) + 1);
    }

    private void init(SharedPreferences sharedPreferences, Bundle bundle) {
        this.sharedPreferences = sharedPreferences;
        gson = new GsonBuilder().create();
        badDays = new ArrayList<>();
        weekendDays = new ArrayList<>();
        goodDays = new ArrayList<>();
        nullDays = new ArrayList<>();
        currentDay = null;
        calendar = Calendar.getInstance();

        //Проверяем чей календарь мы должны показать
        if (bundle != null) {
            //Сотрудника
            String jsonWorker = bundle.getString(Constants.TAG_INTENT_WORKER);
            Worker worker = gson.fromJson(jsonWorker, Worker.class);
            curYear = worker.getMarks();
        } else {
            //Пользователя
            String usersJson = sharedPreferences.getString(Constants.TAG_USERS_JSON, null);
            List<User> allUsers = gson.fromJson(usersJson, new TypeToken<List<User>>(){}.getType());
            for (User user : allUsers){
                if (user.isAuth()) {
                    curYear = user.getMarks();
                    break;
                }
            }
        }
    }

    @Override
    protected void onFirstViewAttach() {
        onDateSelected(CalendarDay.from(LocalDate.now()));
        super.onFirstViewAttach();
    }

    private void fillCalendar(int month) {
        CalendarDay firstDay = TimeUtils.getFirstWorkDayOfYear(curYear);
        if (firstDay == null) return;

        weekendDays.clear();
        nullDays.clear();
        badDays.clear();
        goodDays.clear();
        currentDay = null;

        CalendarDay today = CalendarDay.from(LocalDate.now());

        for (Map.Entry<Integer, Day> dayOfMonth : curYear.getMonth(month).getDays().entrySet()) {
            CalendarDay calDay = CalendarDay.from(curYear.getYear(), month, dayOfMonth.getKey());
            DayOfWeek dayOfWeek = calDay.getDate().getDayOfWeek();
            long hours = TimeUtils.getHoursOfDay(dayOfMonth.getValue());

           if (calDay.equals(today)) {
                currentDay = today;
           } else if (hours >= 8) {
               goodDays.add(calDay);
           } else if (dayOfWeek == DayOfWeek.SATURDAY
                    || dayOfWeek == DayOfWeek.SUNDAY) {
               if (dayOfMonth.getValue().getMarks().size() != 0)
                   goodDays.add(calDay);
               else
                   weekendDays.add(calDay);
           } else if (TimeUtils.getDayTimeLong(dayOfMonth.getValue()) > 0) {
               badDays.add(calDay);
           } else if (((calDay.isBefore(today) && calDay.isAfter(firstDay))
                   || calDay.equals(firstDay))
                   && dayOfMonth.getValue().getMarks().size() == 0) {
                    nullDays.add(calDay);
            }
        }
        getViewState().addDecorators(weekendDays, nullDays, badDays, goodDays, currentDay);
    }

    public void onMonthChanged(int monthOfYear) {
        fillCalendar(monthOfYear);
    }

    public void onDateSelected(CalendarDay selectedDay) {
        if (curYear == null) return;
        Month curMonth = curYear.getMonth(selectedDay.getMonth());
        String dayTime = TimeUtils.getDayTimeString(curMonth.getDay(selectedDay.getDay()));
        String monthTime = TimeUtils.getMonthTimeString(curMonth);
        getViewState().showTime(dayTime, monthTime);
    }

    public void onInfDayClick(CalendarDay selectedDay) {
        int month = selectedDay.getMonth();
        int dayOfMonth = selectedDay.getDay();
        Day day = curYear.getMonth(month).getDay(dayOfMonth);
        LocalDate calendarDay =  LocalDate.of(
               curYear.getYear(),
                month,
                dayOfMonth);
        String msg;

        if (day.getMarks().size() != 0){
            msg = calendarDay.equals(LocalDate.now())
                    ? Constants.CURRENT_DAY
                    : Constants.WORKED_PER_DAY_MARKS + TimeUtils.getDayTimeString(day);

            getViewState().showMarks(day, msg, true);
        } else {
            msg = calendarDay.getDayOfWeek() == DayOfWeek.SATURDAY
                    || calendarDay.getDayOfWeek() == DayOfWeek.SUNDAY
                    ? Constants.WEEKEND_DAY
                    : Constants.NULL_MARKS;
            getViewState().showMessage(msg);
        }
    }

    public void onColorsClick(boolean visibility) {
        getViewState().showColorsDialog(visibility);
    }
}
