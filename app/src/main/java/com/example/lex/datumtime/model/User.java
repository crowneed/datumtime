package com.example.lex.datumtime.model;

import java.util.Date;

/**
 * Created by lex on 12.09.18.
 */

public class User {
    private String login;
    private String password;
    private boolean saved;
    private Year marks;
    private boolean auth;
    private Date lastAbsence;

    public Date getLastAbsence() {
        return lastAbsence;
    }

    public void setLastAbsence(Date lastAbsence) {
        this.lastAbsence = lastAbsence;
    }

    public boolean isAuth() { return auth; }

    public void setAuth(boolean auth) { this.auth = auth; }

    public Year getMarks() { return marks; }

    public void setMarks(Year marks) { this.marks = marks; }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
