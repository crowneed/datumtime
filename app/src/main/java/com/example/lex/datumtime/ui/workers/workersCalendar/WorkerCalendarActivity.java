package com.example.lex.datumtime.ui.workers.workersCalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.ui.calendar.CalendarFragment;

public class WorkerCalendarActivity
        extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_calendar);

        if(savedInstanceState != null) return;
        Intent intent = getIntent();

        if (intent == null) return;
        String jsonWorker = intent.getStringExtra(Constants.TAG_INTENT_WORKER);

        if(jsonWorker == null) return;

        Bundle bundle = new Bundle();
        bundle.putString(Constants.TAG_INTENT_WORKER, jsonWorker);
        CalendarFragment calendarFragment = new CalendarFragment();
        calendarFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, calendarFragment)
                .commit();
    }
}
