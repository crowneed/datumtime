package com.example.lex.datumtime.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Day {
    private List<Date> marks;
    private int dayOfMonth;
    private int monthOfYear;
    private int year;

    public Day(int dayOfMonth,
               int monthOfYear,
               int year) {
        marks = new ArrayList<>();
        this.dayOfMonth = dayOfMonth;
        this.monthOfYear = monthOfYear;
        this.year = year;
    }

    public List<Date> getMarks() {
        return marks;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public int getMonthOfYear() {
        return monthOfYear;
    }

    public int getYear() {
        return year;
    }
}
