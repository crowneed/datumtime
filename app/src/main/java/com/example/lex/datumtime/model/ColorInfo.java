package com.example.lex.datumtime.model;

public class ColorInfo {
    private int colorRes;
    private String colorInfo;

    public ColorInfo(int colorRes,
                     String colorInfo) {
        this.colorRes = colorRes;
        this.colorInfo = colorInfo;
    }

    public int getColorRes() {
        return colorRes;
    }

    public void setColorRes(int colorRes) {
        this.colorRes = colorRes;
    }

    public String getColorInfo() {
        return colorInfo;
    }

    public void setColorInfo(String colorInfo) {
        this.colorInfo = colorInfo;
    }
}
