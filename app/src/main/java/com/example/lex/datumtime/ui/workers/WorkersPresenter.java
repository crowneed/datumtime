package com.example.lex.datumtime.ui.workers;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.lex.datumtime.model.Worker;
import com.example.lex.datumtime.model.Year;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@InjectViewState
public class WorkersPresenter
        extends MvpPresenter<WorkersView> {
    private List<Worker> workers;
    private Calendar calendar;
    private Gson gson;

    /**
     * Вызываем метод fillWorkers()
     */
    WorkersPresenter(){
        workers = new ArrayList<>();
        calendar = Calendar.getInstance();
        gson = new GsonBuilder().create();
        fillWorkers();
    }

    /**
     * Передаем команду в WorkersFragment
     * инициализировать адаптер списком workers
     * @param view
     */
    @Override
    public void attachView(WorkersView view) {
        super.attachView(view);
        getViewState().initAdapter(workers);
    }

    /**
     * Инициализируем список workers
     */
    private void fillWorkers() {
        Worker worker;
        Calendar testCalendar = Calendar.getInstance();
        workers.add(createFakeWorker("Кирьянов Е.", 2, 11, 15, 14, 5));
        workers.add(createFakeWorker("Катрыч А.", 3, 11, 15, 11, 6));
        workers.add(createFakeWorker("Некрасов С.", 3, 11, 15, 11, 7));
        workers.add(createFakeWorker("Курбатов В.", 3, 18, 21, 9, 9));

        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int month = Calendar.getInstance().get(Calendar.MONTH);

        worker = createFakeWorker("Швидченко А.", 3, 18, 21, 9, 10);
        testCalendar.set(2019, month, day, 9,0);
        worker.getMarks().getMonth(month+1).getDay(day).getMarks().add(testCalendar.getTime());
        testCalendar.set(2019, month, day, 17,35);
        worker.getMarks().getMonth(month+1).getDay(day).getMarks().add(testCalendar.getTime());
        workers.add(worker);

        worker = createFakeWorker("Манько А.", 2, 11, 15, 11, 9);
        testCalendar.set(2019, month, day, 11,0);
        worker.getMarks().getMonth(month+1).getDay(day).getMarks().add(testCalendar.getTime());
        workers.add(worker);
    }

    private Worker createFakeWorker(String name, int month, int firstDay, int lastDay, int startHourDay, int durationHour) {
        Year year = new Year(2019);
        Worker worker = new Worker();
        worker.setName(name);

        for (int i = firstDay; i <= lastDay; i++) {
            calendar.set(year.getYear(),month - 1, i, startHourDay,0);
            List<Date> marks = year.getMonth(month).getDay(i).getMarks();
            marks.add(calendar.getTime());
//            year.getMonth(month).getDay(i).getMarks().add(calendar.getTime());
            calendar.add(Calendar.HOUR_OF_DAY, durationHour);
            year.getMonth(month).getDay(i).getMarks().add(calendar.getTime());
        }
        worker.setMarks(year);
        return worker;
    }


    public void onWorkerClick(Worker worker) {
        String jsonWorker = gson.toJson(worker);
        getViewState().showCalendar(jsonWorker);
    }
}
