package com.example.lex.datumtime.ui.workers;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.lex.datumtime.model.Worker;

import java.util.List;

public interface WorkersView
        extends MvpView {

    /**
     * Инициализируем адаптер и передаем в него
     * список @param workers
     */
    @StateStrategyType(SkipStrategy.class)
    void initAdapter(List<Worker> workers);

    @StateStrategyType(SkipStrategy.class)
    void showCalendar(String jsonWorker);

}
