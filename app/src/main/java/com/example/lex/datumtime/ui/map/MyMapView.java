package com.example.lex.datumtime.ui.map;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;

public interface MyMapView
        extends MvpView {

    /**
     * Устанавливаем уровень зума для карты
     * @param zoomLevel
     */
    @StateStrategyType(SkipStrategy.class)
    void setZoomMap(double zoomLevel);

    /**
     * Устанавливаем положение пользователя на карте
     * @param gpt
     */
    @StateStrategyType(SkipStrategy.class)
    void setCurrentLocation(GeoPoint gpt);

    /**
     * Рисуем зону на карте с точками
     * @param circlePoints
     */
    @StateStrategyType(SkipStrategy.class)
    void drawZone(ArrayList<GeoPoint> circlePoints);

    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);

    @StateStrategyType(SkipStrategy.class)
    void makeActiveButton(boolean makeActive, long time);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showAbsenceDialog(boolean visibility);

    //VIEW.VISIBILITY
    @StateStrategyType(SkipStrategy.class)
    void showButtonAbsence(int visibility);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showProgressSend(boolean visibility);
}
