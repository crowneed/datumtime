package com.example.lex.datumtime.Utils.calendarUltis;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;



public class ViewDecorator
        implements DayViewDecorator {
    
        private HashSet<CalendarDay> dates;
        private CalendarDay date;
        private int color;


        public ViewDecorator(final Context context, int color, Collection<CalendarDay> dates) {
            this.color = ContextCompat.getColor(context, color);
            this.dates = new HashSet<>(dates);
        }

    public ViewDecorator(final Context context, int color, CalendarDay day) {
        date = day;
        this.color = ContextCompat.getColor(context, color);
    }

    @Override
        public boolean shouldDecorate(CalendarDay day) {
            boolean isShould = false;
            if (dates == null && date !=null) isShould = day.equals(date);
            else if (dates != null) isShould = dates.contains(day);
            return isShould;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new LineSpan(5, color));
        }
}
