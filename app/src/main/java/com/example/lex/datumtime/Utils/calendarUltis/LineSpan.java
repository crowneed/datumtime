package com.example.lex.datumtime.Utils.calendarUltis;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

public class LineSpan implements LineBackgroundSpan {
    private final float DEFAULT_HEIGHT = 5.0F;
    private final float BASE_PADDING = 5.0F;
    private final int DEFAULT_COLOR = Color.RED;
    private final float height;
    private final int color;

    public LineSpan(float height, int color) {
        this.height = height;
        this.color = color;
    }

    public LineSpan(int color) {
        this.color = color;
        this.height = DEFAULT_HEIGHT;
    }

    public LineSpan(float height) {
        this.height = height;
        this.color = DEFAULT_COLOR;
    }

    public LineSpan() {
        this.height = DEFAULT_HEIGHT;
        this.color = DEFAULT_COLOR;
    }

    @Override
    public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
        int oldColor = p.getColor();
        p.setColor(this.color);

        c.drawRect((float)left + BASE_PADDING,
                (float)bottom + BASE_PADDING,
                (float)right - BASE_PADDING,
                (float)bottom + BASE_PADDING + height,
                p);
        p.setColor(oldColor);
    }
}
