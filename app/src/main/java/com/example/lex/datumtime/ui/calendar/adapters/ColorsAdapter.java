package com.example.lex.datumtime.ui.calendar.adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lex.datumtime.R;
import com.example.lex.datumtime.model.ColorInfo;

import java.util.List;

public class ColorsAdapter
        extends RecyclerView.Adapter<ColorsAdapter.ViewHolder> {
    private List<ColorInfo> colors;

    public ColorsAdapter(List<ColorInfo> colors) {
        this.colors = colors;
    }

    @NonNull
    @Override
    public ColorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,
                                                       int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_color,
                        viewGroup,
                        false);
        return new ColorsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsAdapter.ViewHolder viewHolder,
                                 int i) {
        final ColorInfo colorInfo = colors.get(i);
        Resources resources = viewHolder.itemView.getResources();
        viewHolder.tvInfo.setText(colorInfo.getColorInfo());
        viewHolder.ivColor.setColorFilter(resources.getColor(colorInfo.getColorRes()));
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder {
        private TextView tvInfo;
        private ImageView ivColor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvInfo = itemView.findViewById(R.id.tv_info);
            ivColor = itemView.findViewById(R.id.iv_color);
        }
    }
}

