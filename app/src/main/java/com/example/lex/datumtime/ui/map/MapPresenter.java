package com.example.lex.datumtime.ui.map;


import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.view.View;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.emailUtils.GMailSender;
import com.example.lex.datumtime.model.User;
import com.example.lex.datumtime.model.Year;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@InjectViewState
public class MapPresenter
        extends MvpPresenter<MyMapView> {
    private double zoomLevel;
    private GeoPoint userPoint;
    private GeoPoint centerZone;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private ArrayList<GeoPoint> zonePoints;
    private Polygon zoneGeom;
    private boolean makeActive;
    private Gson gson;
    private SharedPreferences sharedPreferences;
    private User currentUser;
    List<User> users;
    String usersJson;

    public MapPresenter(FusedLocationProviderClient mFusedLocationProviderClient,
                        SharedPreferences sharedPreferences) {
        init(mFusedLocationProviderClient,
                sharedPreferences);
    }

    /**
     * Вызываем метод getCurrentLocation()
     * и передаем команды в MapFragment
     * setZoomMap() и drawZone()
     * @param view
     */
    @Override
    public void attachView(MyMapView view) {
        super.attachView(view);

        getCurrentLocation();
        getViewState().setZoomMap(zoomLevel);
        getViewState().drawZone(zonePoints);


        /**
         * код часто повторяется - нужно подумать как это исправить
         */
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        usersJson = sharedPreferences.getString(Constants.TAG_USERS_JSON, null);
        users = gson.fromJson(usersJson, new TypeToken<List<User>>(){}.getType());
        Year curYear = null;
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).isAuth()) {
                currentUser = users.get(i);
                curYear = currentUser.getMarks();
                if (curYear == null) {
                    curYear = new Year(Calendar.getInstance().get(Calendar.YEAR));
                }
                if (currentUser.getLastAbsence() != null) {
                    calendar.setTime(currentUser.getLastAbsence());
                    CalendarDay today = CalendarDay.from(curYear.getYear(), month + 1, day);
                    CalendarDay lastAbsence = CalendarDay.from(
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH) + 1,
                            calendar.get(Calendar.DAY_OF_MONTH));

                    if (today.equals(lastAbsence)) {
                        getViewState().showButtonAbsence(View.GONE);
                    } else {
                        getViewState().showButtonAbsence(View.VISIBLE);
                    }
                }

                long time;
                makeActive = false;
                List<Date> marks = curYear.getMonth(month+1).getDay(day).getMarks();

                if (marks.size() % 2 != 0) {
                    makeActive = true;
                    long sumOdd = 0;
                    long sumEven = 0;
                    for (int j = 0; j < marks.size(); j++) {
                        if (j % 2 == 0) sumOdd += marks.get(j).getTime();
                        else sumEven += marks.get(j).getTime();
                    }
                    sumEven+= new Date().getTime();
                    time = sumEven - sumOdd;
                    getViewState().makeActiveButton(makeActive, time);
                } else if (marks.size() != 0) {
                    long sumOdd = 0;
                    long sumEven = 0;
                    for (int j = 0; j < marks.size(); j++) {
                        if (j % 2 == 0) sumOdd += marks.get(j).getTime();
                        else sumEven += marks.get(j).getTime();
                    }
                    time = sumEven - sumOdd;
                    getViewState().makeActiveButton(makeActive, time);
                }
                break;
            }
        }
    }

    /**
     * При продолжении фрагмента мы начинаем обновлять
     * данные пользователя
     */
    public void onResumeFragment() {
        startLocationUpdates();
    }

    /**
     * При паузе фрагмента перестаем обновлять текущее
     * местоположение пользователя
     */
    public void onPauseFragment() {
        stopLocationUpdates();
    }

    /**
     * Сохраняем уровень зума, при уничтожении активити
     * @param zoomLevel
     */
    public void onDestroyFragment(double zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    /**
     * Устанавливаем уровень зума по умолчанию
     * Определяем точки зоны
     * Устанавливаем интервал обновления местоположения пользователя
     * @param mFusedLocationProviderClient
     */
    private void init(FusedLocationProviderClient mFusedLocationProviderClient,
                      SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        gson = new GsonBuilder().create();
        this.mFusedLocationClient = mFusedLocationProviderClient;
        zoomLevel = Constants.DEFAULT_ZOOM_LEVEL;
        centerZone = new GeoPoint(Constants.CENTER_ZONE_LAT, Constants.CENTER_ZONE_LNG);
        onCreateZone(centerZone, Constants.ZONE_RADIUS_IN_METERS);

        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) return;
                Location location = locationResult.getLocations().get(0);
                onResult(location);
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.AVERAGE_INTERVAL_IN_MS);
        mLocationRequest.setFastestInterval(Constants.MAX_INTERVAL_IN_MS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Определяем крайние точки зоны
     * с центром зоны в @param centerZone
     * и с радиуом зоны @param radiusInMeters
     * Точки берем через каждые MIN_POINT_DEGREE
     */
    private void onCreateZone(GeoPoint centerZone,
                              double radiusInMeters) {
        zonePoints = new ArrayList<>(
                Constants.MAX_CIRCLE_DEGREE / Constants.MIN_POINT_DEGREE);
        Coordinate polygon[] = new Coordinate[(Constants.MAX_CIRCLE_DEGREE / Constants.MIN_POINT_DEGREE)+1];
        int index = 0;
        for (int i = 0;
             i < Constants.MAX_CIRCLE_DEGREE;
             i += Constants.MIN_POINT_DEGREE) {
            GeoPoint onZone = centerZone.destinationPoint(radiusInMeters, i);
            Coordinate coordinate = new Coordinate(onZone.getLatitude(),onZone.getLongitude());
            polygon[index] = coordinate;
            zonePoints.add(onZone);
            index++;
        }
        polygon[index] = polygon[0];
        zoneGeom = new GeometryFactory().createPolygon(polygon);
    }

    /**
     * Делаем запрос на определение текущего местоположения,
     * при успешном запросе вызываем onResult()
     */
    private void getCurrentLocation() {

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        onResult(location);
                    }
                });
    }

    /**
     * Передаем команду в MapFragment установить
     * текущее местоположение пользователя
     * с полученной точкой из @param location
     */
    private void onResult(Location location) {
        if (location == null) return;
        userPoint = new GeoPoint(location.getLatitude(),
                location.getLongitude());
        getViewState().setCurrentLocation(userPoint);
    }

    /**
     * Запускаем обновление данных
     * о текущем местоположении пользователя
     */
    private void startLocationUpdates() {

        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback,
                null);
    }

    /**
     * Останавливаем обновление данных
     * о текущем местоположении пользователя
     */
    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void checkPoint() {
        Coordinate userPointCoordinate = new Coordinate(
                userPoint.getLatitude(),
                userPoint.getLongitude());
        Geometry userPointGeom = new GeometryFactory().createPoint(userPointCoordinate);
        if (zoneGeom.contains(userPointGeom)) {
            saveCheckPoint();
        } else {
            getViewState().showMessage("Вы за пределами зоны!");
        }
    }

    private void saveCheckPoint() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String usersJson = sharedPreferences.getString(Constants.TAG_USERS_JSON, null);
        List<User> users = gson.fromJson(usersJson, new TypeToken<List<User>>(){}.getType());
        Year curYear = null;
        for(User user: users){
            if (user.isAuth()) {
                curYear = user.getMarks();
                if (curYear == null) {
                    curYear = new Year(Calendar.getInstance().get(Calendar.YEAR));
                }

                curYear.getMonth(month+1).getDay(day).getMarks().add(new Date());
                user.setMarks(curYear);
                usersJson = gson.toJson(users);
                sharedPreferences.edit().putString(Constants.TAG_USERS_JSON, usersJson).apply();

                long time;
                makeActive = false;
                List<Date> marks = curYear.getMonth(month+1).getDay(day).getMarks();

                if (marks.size() % 2 != 0) {
                    makeActive = true;
                    if (marks.size() == 1) {
                        getViewState().makeActiveButton(makeActive, 0);
                    } else {
                    long sumOdd = 0;
                    long sumEven = 0;
                    for (int i = 0; i < marks.size()-1; i++) {
                        if(i % 2 == 0) sumOdd+= marks.get(i).getTime();
                        else sumEven += marks.get(i).getTime();
                    }
                    time = sumEven - sumOdd;
                    getViewState().makeActiveButton(makeActive, time);
                    }
                } else {
                    long sumOdd = 0;
                    long sumEven = 0;
                    for (int i = 0; i < marks.size()-1; i++) {
                        if(i % 2 == 0) sumOdd+= marks.get(i).getTime();
                        else sumEven += marks.get(i).getTime();
                    }
                    sumEven+= new Date().getTime();
                    time = sumEven - sumOdd;
//                    getViewState().makeActiveButton(makeActive,-1);
                    getViewState().makeActiveButton(makeActive, time);
                }
                break;
            }
        }
    }

    public void onSendClick() {
        getViewState().showAbsenceDialog(true);
    }

    public void onAbsenceClick(final String absence) {
        getViewState().showProgressSend(true);
        getViewState().showButtonAbsence(View.INVISIBLE);
        new MailTask(absence).execute();
    }

    private class MailTask extends AsyncTask<Void, Void, String> {
        private String absence;

        public MailTask(String absence) {
            this.absence = absence;
        }

        @Override
        protected String doInBackground(Void... voids) {
            GMailSender sender = new GMailSender(Constants.MAIL_SENDER,
                    Constants.MAIL_SENDER_PASSWORD);
            try {
                boolean isSended =  sender.sendMail(
                        Constants.MAIL_MESSAGE_TITLE,
                        absence,
                        Constants.MAIL_SENDER,
                        Constants.MAIL_MESSAGE_RECIPIENT);
                if (isSended)
                    return Constants.MAIL_SENDED;
                else
                    return Constants.MAIL_NOT_SENDED;
            } catch (Exception e){
                return e.getMessage();
            }
        }

        @Override
            protected void onPostExecute(String messageResult) {
            getViewState().showProgressSend(false);
            if (messageResult.equals(Constants.MAIL_SENDED)){
                currentUser.setLastAbsence(new Date());
                usersJson = gson.toJson(users);
                sharedPreferences.edit().putString(Constants.TAG_USERS_JSON, usersJson).apply();
                getViewState().showButtonAbsence(View.GONE);
            } else {
                getViewState().showButtonAbsence(View.VISIBLE);
            }
            getViewState().showMessage(messageResult);
        }
    }
}
