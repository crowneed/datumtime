package com.example.lex.datumtime.ui.auth;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.ui.main.MainActivity;


public class AuthActivity
        extends MvpAppCompatActivity
        implements AuthView {

    @InjectPresenter
    AuthPresenter authPresenter;

    /**
     * Передаем в AuthPresenter SharedPreferences
     */
    @ProvidePresenter
    AuthPresenter provideMainPresenter(){
        return new AuthPresenter(
                getSharedPreferences(Constants.TAG_SHARED_PREFERENCES,
                MODE_PRIVATE));
    }

    private EditText etLogin, etPassword;
    private Button btnLogin;
    private TextView tvError;
    private CheckBox boxSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

/**
 * Делаем проверку разрешений, необходимых для корректной работы приложения
 */
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_REQUEST);
        }

        boxSave = findViewById(R.id.cb_save);
        etLogin = findViewById(R.id.et_login);
        etPassword = findViewById(R.id.et_pas);
        tvError = findViewById(R.id.tv_error);
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authPresenter.onLogin(
                        etLogin.getText().toString(),
                        etPassword.getText().toString(),

                        boxSave.isChecked());
            }
        });

    }

    /**
     * Показываем сообщение(ошибку) пользователю
     */
    @Override
    public void showMessage(String message) {
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
    }

    /**
     * Переходим в MainActivity
     */
    @Override
    public void login() {
        Intent intent = new Intent(
                AuthActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }
}
