package com.example.lex.datumtime.ui.map;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.lex.datumtime.R;
import com.example.lex.datumtime.Utils.Constants;
import com.example.lex.datumtime.Utils.TimeUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayManager;
import org.osmdroid.views.overlay.Polygon;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MapFragment
        extends MvpAppCompatFragment
        implements MyMapView {

    @InjectPresenter
    MapPresenter mapPresenter;

    /**
     * Передаем в MapPresenter FusedLocationProviderClient
     * @return
     */
    @ProvidePresenter
    MapPresenter provideMyMapPresenter(){
        Context context = getActivity().getApplicationContext();
        FusedLocationProviderClient mFusedLocationClient =
                LocationServices.getFusedLocationProviderClient(context);
        return new MapPresenter(mFusedLocationClient,
                getActivity().getSharedPreferences(
                        Constants.TAG_SHARED_PREFERENCES,
                        Context.MODE_PRIVATE));
    }

    private MapView map;
    private MapController mapController;
    private OverlayManager overlays;
    private Marker userPoint;
    private Polygon zone;
    private Button btnCheckPoint, btnSend;
    private Chronometer chronometer;
    private Resources resources;
    private ProgressBar pbSend;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map,null);
        Configuration
                .getInstance()
                .load(
                        getActivity().getApplicationContext(),
                        PreferenceManager
                                .getDefaultSharedPreferences(getActivity().getApplicationContext()));
        init(view);
        return view;
    }

    /**
     * Инициализируем все поля текущего фрагмента
     * отрисовываем карту
     * @param view
     */
    private void init(View view) {
        chronometer = view.findViewById(R.id.chronometer);
//        chronometer.setFormat(Constants.CHRONO_FORMAT);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                cArg.setText(TimeUtils.getTimeinChronoFormat(time));
            }
        });
        pbSend = view.findViewById(R.id.pb_send);
        btnCheckPoint = view.findViewById(R.id.btn_check);
        btnCheckPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapPresenter.checkPoint();
            }
        });
        btnSend = view.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapPresenter.onSendClick();
            }
        });
        resources = this.getResources();
        map = view.findViewById(R.id.map);
        mapController = (MapController) map.getController();
        overlays = map.getOverlayManager();
        map.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
        map.setMultiTouchControls(true);
        map.setBuiltInZoomControls(false);
        zone = new Polygon(map);
        zone.setInfoWindow(null);
        zone.setStrokeColor(resources.getColor(R.color.colorAccent));
        zone.setFillColor(resources.getColor(R.color.colorPrimaryO50));
        zone.setStrokeWidth(Constants.STROKE_ZONE_WIDTH);
        overlays.add(zone);
        userPoint = new Marker(map);
        userPoint.setInfoWindow(null);
        overlays.add(userPoint);
        userPoint.setIcon(resources.getDrawable(R.drawable.user_point));
    }

    /**
     * Добавляем в полигон точки @param zonePoints
     */
    @Override
    public void drawZone(ArrayList<GeoPoint> zonePoints) {
        zone.setPoints(zonePoints);
        map.invalidate();
    }

    @Override
    public void makeActiveButton(boolean makeActive, long time) {
        chronometer.setVisibility(View.VISIBLE);
        int drawableId;
        String textBtn;
        if (makeActive) {
            chronometer.setBase(SystemClock.elapsedRealtime() - time);
            chronometer.setText(TimeUtils.getTimeinChronoFormat(time));
            chronometer.start();
            drawableId = R.drawable.button_active_main;
            textBtn = Constants.CHECK_OUT;
        } else {
            if (time != -1) {
                chronometer.setBase(SystemClock.elapsedRealtime() - time);
                chronometer.setText(TimeUtils.getTimeinChronoFormat(time));
            }
            chronometer.stop();
            drawableId = R.drawable.button_passive_main;
            textBtn = Constants.CHECK_IN;
        }
        btnCheckPoint.setBackgroundResource(drawableId);
        btnCheckPoint.setText(textBtn);
    }

    @Override
    public void showAbsenceDialog(boolean visibility) {
        if(visibility){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
            View parentView = getLayoutInflater().inflate(R.layout.bottom_sheet_absence, null);
            bottomSheetDialog.setContentView(parentView);
            final EditText etAbsence = parentView.findViewById(R.id.et_absence);
            final TextView tvAbsenceError = parentView.findViewById(R.id.tv_absence_error);
            etAbsence.setImeOptions(EditorInfo.IME_ACTION_DONE);
            etAbsence.setRawInputType(InputType.TYPE_CLASS_TEXT);
            Button btnAbsence = parentView.findViewById(R.id.btn_send_absence);
            btnAbsence.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String absence = etAbsence.getText().toString();
                    if(absence.isEmpty()) {
                        tvAbsenceError.setVisibility(View.VISIBLE);
                        return;
                    }
                    mapPresenter.onAbsenceClick(absence);
                    bottomSheetDialog.dismiss();
                }
            });
//            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View)parentView.getParent());
//            bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics()));
            bottomSheetDialog.show();
            bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mapPresenter.getViewState().showAbsenceDialog(false);
                }
            });
        }
    }

    @Override
    public void showButtonAbsence(int visibility) {
        if (visibility != View.VISIBLE
                && visibility != View.GONE
                && visibility != View.INVISIBLE)
            return;
        btnSend.setVisibility(visibility);
    }

    @Override
    public void showProgressSend(boolean visibility) {
        if (visibility) {
            pbSend.setVisibility(View.VISIBLE);
        } else {
            pbSend.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessage(String message) {
        int yOffset = chronometer.getVisibility() == View.VISIBLE
                ? 140
                : 10;
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP,0, yOffset);
        toast.show();
    }

    /**
     * Устанавливаем уровень зума для карты
     * @param zoomLevel
     */
    @Override
    public void setZoomMap(double zoomLevel) {
        mapController.setZoom(zoomLevel);
    }

    /**
     * Устанавливаем положение пользователя на карте
     * @param gpt
     */
    @Override
    public void setCurrentLocation(GeoPoint gpt) {
        mapController.setCenter(gpt);
        userPoint.setPosition(gpt);
        map.invalidate();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapPresenter.onResumeFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapPresenter.onPauseFragment();
    }

    @Override
    public void onDestroyView() {
        mapPresenter.onDestroyFragment(
                map.getZoomLevelDouble());
        super.onDestroyView();
    }
}
