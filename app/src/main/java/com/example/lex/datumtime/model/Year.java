package com.example.lex.datumtime.model;

import java.util.HashMap;
import java.util.Map;

public class Year {
    private Map<Integer, Month> months = new HashMap<>();
    private final int year;

    public Year(int year) {
        this.year = year;
        for(int i = 1; i <= 12; i++) {
            int amountDays;
            if ((i % 2 != 0 && i <= 7)
                    || (i % 2 == 0 && i >= 8)) {
                amountDays = 31;
            } else if (i == 2) {
                if (year % 4 == 0)
                    amountDays = 29;
                else
                    amountDays = 28;
            } else {
                amountDays = 30;
            }
            months.put(i, new Month(amountDays, i, year));
        }
    }

    public Map<Integer, Month> getMonths() {
        return months;
    }

    public Month getMonth(int i){
        return months.get(i);
    }

    public int getYear() {
        return year;
    }
}
